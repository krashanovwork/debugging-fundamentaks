﻿using System;

namespace Task1
{
    public class Product
    {
        public Product(string name, double price)
        {
            Name = name;
            Price = price;
        }

        public string Name { get; set; }

        public double Price { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as Product);
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Price);
        }

        public bool Equals(Product product)
        {
            return product != null && Name == product.Name && Price == product.Price;
        }
    }
}
